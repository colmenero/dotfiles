Switching modes
===============

You can use `<Ctrl> + <Shift> + <Space>` to switch between insert and normal
modes (just like in vim). Once you are in normal mode you can scroll up, down,
or visually select and copy text.
