# Terminator by Chris Jones <cmsj@tenshu.net?
# GPL v2 only
"""url_handlers.py - New URL handlers"""
import re
import terminatorlib.plugin as plugin

# Every plugin you want Terminator to load *must* be listed in 'AVAILABLE'
AVAILABLE = ['JiraIssueHandler']

class JiraIssueHandler(plugin.URLHandler):
    """Launchpad Bug URL handler. If the URL looks like an RTI Jirra Issue
    closure entry... 'feature/PROJECT-1234' then it should be transformed into
    a Jira URL"""
    capabilities = ['url_handler']
    handler_name = 'rti_jira_handler'
    match = r'[A-Z]+-[0-9]+'
    nameopen = "Open Jira issue"
    namecopy = "Copy Jira URL"

    def callback(self, url):
        """Look for the Issue in the supplied string and return it as a URL"""
        for item in re.findall(r'[A-Z]+-[0-9]+', url):
            url = 'jira.rti.com:8443/browse/%s' % item
            return(url)
