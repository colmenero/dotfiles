#======================================================#
# ACCOUNT DETAILS
# ===============

set realname = "Luis Colmenero Sendra"
set use_from = yes
set envelope_from ="yes"
set sendmail="/usr/bin/msmtp"

# Default account
source "~/.mutt/account.google"
folder-hook $folder 'source ~/.mutt/account.google'
macro index <f2> '<sync-mailbox><enter-command>source ~/.mutt/account.google<enter><change-folder>!<enter>'

# Link to other accounts
folder-hook imaps://imap.gmail.com:993/ 'source ~/.mutt/account.uc3m'
macro index <f3> '<sync-mailbox><enter-command>source ~/.mutt/account.uc3m<enter><change-folder>!<enter>'
folder-hook imaps://imap-mail.outlook.com:993/ 'source ~/.mutt/account.microsoft'
macro index <f4> '<sync-mailbox><enter-command>source ~/.mutt/account.microsoft<enter><change-folder>!<enter>'
folder-hook imaps://correo.ugr.es:993/ 'source ~/.mutt/account.ugr'
macro index <f5> '<sync-mailbox><enter-command>source ~/.mutt/account.ugr<enter><change-folder>!<enter>'

#======================================================#
# CONNECTION DETAILS
# ==================

# Store message headers locally to speed things up
set header_cache = ~/.cache/mutt

# Store messages locally to speed things up (like
# searching message bodies).This will cost important
# disk usage according to your e-mail amount.
set message_cachedir = "~/.cache/mutt"

# Allow Mutt to open new imap connection automatically.
unset imap_passive

# Keep IMAP connection alive by polling intermittently
# (time in seconds).
set imap_keepalive = 300

# How often to check for new mail (time in seconds).
set mail_check = 120

#======================================================#
# WATCH THESE MAILBOXES FOR NEW MAIL
# ==================================
mailboxes ! +Fetchmail +slrn +mutt
set sort_browser=alpha    # Sort mailboxes by alpha(bet)

#======================================================#
# ORDER OF HEADERS AND WHAT TO SHOW
# =================================

hdr_order Date: From: User-Agent: X-Mailer \
          To: Cc: Reply-To: Subject:
ignore *
unignore Date: From: User-Agent: X-Mailer  \
         To: Cc: Reply-To: Subject:

# Group threads
set sort=threads
set sort_aux=last-date-received

#======================================================#
# EDITOR
# ======

set edit_headers     # See the headers when editing
set text_flowed=yes  # Manage line breaks.
                     # Comments on .vim/after/ftpuglin/mail.vi

#======================================================#
# ALIASES
# =======

set sort_alias=alias     # sort aliases in alpha order by alias name

#======================================================#
# THEME
# =====
source ~/.mutt/colors.solarized

#======================================================#
# CONTACTS (through abook)
# ========================
set query_command= "abook --datafile /home/luis/Documents/contactsBookmarksAndCalendars/Contacts.abook --mutt-query '%s'"
macro index,pager  A "<pipe-message>abook --add-email-quiet<return>" "Add this sender to Abook"
bind editor        <Tab> complete-query

#======================================================#
# HTML MAILS (through w3m)
# ========================
# Create mailcap file before
set mailcap_path 	= ~/.mutt/mailcap
auto_view text/html
alternative_order text/plain text/html

# Fallback (no me funca)
# The following macro will open the HTML mail selected from the
# attachment view in the web browser defined in the environment
#macro attach 'V' "<pipe-entry>cat >~/.cache/mutt/mail.html && $BROWSER ~/.cache/mutt/mail.html && rm ~/.cache/mutt/mail.html<enter>"

#======================================================#
# GPG (encryption and signing) # in progress
# ============================
# Comandos basicos por defecto necesarios para usar gpg.
# Equivalente al gpg.rc que hay en el git
source ~/.mutt/gpg.rc

set pgp_sign_as=492BDD76
unset pgp_autosign
unset pgp_replysign
unset pgp_autoencrypt
set pgp_replyencrypt
set pgp_verify_sig=yes
set pgp_timeout = 1800

# key binds
#   Con la tecla p podemos abrir un menu desde que encriptar/firmar
bind compose p  pgp-menu

#======================================================#
# ODDS AND ENDS
# =============

set markers          # mark wrapped lines of text in the pager with a +
set smart_wrap       # Don't wrap mid-word
set pager_context=5  # Retain 5 lines of previous page when scrolling.

set status_on_top    # Status bar on top.
push <show-version>  # Shows mutt version at startup
macro index 'c' '<change-folder>?<change-dir><home>^K=<enter>' # List current mailbox folders when pressing c key
set sleep_time=0 # Changing between folders faster
# Up/down keys scroll inside message, not inside inbox
bind pager <up> previous-line
bind pager <down> next-line
