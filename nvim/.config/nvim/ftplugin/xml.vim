" Indentation
" -----------
set expandtab     " insert spaces when hitting TABs
set shiftround    " round indent to multiple of 'shiftwidth'
set autoindent    " align the new line indent with the previous line
set cindent       " Stricter rules for c-programs (needs autoindent set)
set shiftwidth=4  " operation >> indents 4 columns; << unindents 4 columns
set tabstop=4     " a hard TAB displays as 4 columns
set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set textwidth=79  " lines longer than 79 columns will be broken

" Xml verification
" ----------------
function! CheckNoHttps(schema_nocheck)
    " xmllint does not support https. If https url given replace with http and
    " retry
    if a:schema_nocheck[0:4] == 'https'
        let schema_check = 'http' . a:schema_nocheck[5:]
    else
        let schema_check = a:schema_nocheck
    endif
    return schema_check
endfunction

function! XML_get_schema()
    " Search for schema location and returns its line
    let schemaLineNumber = search('xsi:noNamespaceSchemaLocation=', 'n')
    let schemaLine = getline(schemaLineNumber)
    let schema_arr = matchlist(schemaLine, 'xsi:noNamespaceSchemaLocation="\(.*\)"')
    if len(schema_arr) > 0
        let schema = CheckNoHttps(schema_arr[1])
    else
        let schema = ''
    endif

    return schema
endfunction

let g:ale_xml_xmllint_options = "--schema " . XML_get_schema()
