" save in .vim/after/ftplugin/
setlocal tw=80
setlocal fo+=aw

" Fuentes:
" http://vimdoc.sourceforge.net/htmldoc/change.html#fo-table
" http://www.mdlerch.com/emailing-mutt-and-vim-advanced-config.html
" http://brianbuccola.com/line-breaks-in-mutt-and-vim/
