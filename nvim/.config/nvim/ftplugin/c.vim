" Save in .vim/after/ftplugin

" Indentation
" -----------
set expandtab     " insert spaces when hitting TABs
set shiftround    " round indent to multiple of 'shiftwidth'
set autoindent    " align the new line indent with the previous line
set cindent       " Stricter rules for c-programs (needs autoindent set)
set shiftwidth=4  " operation >> indents 4 columns; << unindents 4 columns
set tabstop=4     " a hard TAB displays as 4 columns
set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set textwidth=79  " lines longer than 79 columns will be broken

" Automatically closing delimiters
"---------------------------------
" inoremap " ""<left>
" inoremap ' ''<left>
" inoremap ( ()<left>
" inoremap [ []<left>
" inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O

" ------------------------------------------------------------------------------
"                                   DDS-SPECIFICS
" ------------------------------------------------------------------------------

" ALE (only used for xml verification so far, see xml.vim)
" --------------------------------------------------------
" DDS is a really large project, so it is divided in submodules. The submodules
" are compiled separately. Otherwise it would take too much to compile each
" change. Anyway, even compiling only modules there is probably significant
" delay.

" ALE tries to compile according to makefile, but does not find files.
let g:ale_linters = {'c': [], 'cpp': []}  " Deactivate ALE for c code

" Try parsing makefile? There must be a way.
" let g:ale_c_parse_makefile = 1
" if compile_commands.json -> let g:ale_parse_compile_commands = 1
