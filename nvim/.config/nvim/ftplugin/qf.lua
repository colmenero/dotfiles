-- Quickfix file type. This is the filetype set for the quickfix buffer

require('functions/quickfixNextPreviousEntry')

vim.keymap.set("n", "<C-j>", nextQuickfixEntry,
    { noremap = true, desc = "Down in quickfix entry and delete previous buffer"})

vim.keymap.set("n", "<C-k>", previousQuickfixEntry,
    { noremap = true, desc = "Up in quickfix entry and delete previous buffer"})
