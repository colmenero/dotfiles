-- ===========================================================================
--                          LUA CONFIGURATION
-- ===========================================================================
-- Doing set vim.opt.property = value in vimscript is the same as doing
-- doing vim.opt.property = value in lua
--
-- For global variables we do: vim.g.property = value

vim.g.mapleader = ","

vim.opt.ignorecase = true -- required by scmartcase
vim.opt.smartcase = true  -- case sensitive only if uppercase given
vim.opt.showmatch = true  -- show the matching part of the pair for [] {} ()
vim.opt.hidden = true     -- buffers can be hidden (keeping its changes)
                          -- without first writing the buffer to a file
vim.opt.cursorline = true -- Highlight and configure current line
vim.opt.cursorcolumn = true -- Highlight current column

-- Consider letters as well when increasing/decreasing using Ctrl-a and Ctrl-x
vim.opt.nrformats:append({'alpha'})

-- Highlight cursor line and column
vim.opt.cursorline = true
vim.opt.cursorcolumn = true
-- Always highlight the 80th column
vim.opt.colorcolumn = "80"
-- :noh

-- Hidden characters
-- Ex. set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
-- We only do it if editing, not vimdiff. Why? Because of some conflicts with my
-- colorscheme, the dots have a strange background color and the result is
-- complicated to read.
if not vim.api.nvim_win_get_option(0, "diff") then
  vim.opt.list = true -- activate
  vim.opt.listchars = {
    tab = '→ ',
    space = '·',
  }
end

-- Line numbers
-- See actual "hybrid" configuration in vimrc.vim
vim.opt.number = true

-- General indentation
-- -------------------
-- Language-specific indentation is in separated .vim
-- Note: To insert a hard tab use Ctrl-V and then the tab key
vim.opt.expandtab = true  -- insert spaces when hitting tabs
vim.opt.shiftround = true -- round indent to multiple of 'shiftwidth'
vim.opt.shiftwidth = 2    -- operations << >> unindent/indent 2 spaces
vim.opt.tabstop = 2       -- a hard TAB displays as 2 columns
vim.opt.softtabstop = 2   -- insert/delete spaces when hitting a TAB/BACKSPACE

-- Vim Temporary files
-- -------------------
-- Save vim backup and swap files in a global hidden directory called vimtemp
-- (we have to create this folder). That way we wont pollute the working
-- directory.
-- If vimtemp doesnt exist, it will fall back to the current directory, thanks to
-- the ",.". The two slashes at the end avoids collisions when working with files
-- that have the same name (includes full directory).
vim.opt.backupdir = os.getenv("HOME") .. "/.vimtemp,."
vim.opt.dir = os.getenv("HOME") .. "/.vimtemp//,."
vim.opt.undodir = os.getenv("HOME") .. "/.vimtemp//,."

-- Sessions
-- --------
-- Reference: https://dockyard.com/blog/2018/06/01/simple-vim-session-management-part-1
-- The directory has to be previously created
vim.g.sessions_dir = '~/.nvim-sessions'
vim.keymap.set("n", "<Leader>ss", ":mks! " .. vim.g.sessions_dir .. '/',
    { noremap = true, desc = "Save session with <name>"})
vim.keymap.set("n", "<Leader>sr", ":so " .. vim.g.sessions_dir .. '/',
    { noremap = true, desc = "Restore session with <name>"})
vim.keymap.set("n", "<Leader>sd", ":!rm " .. vim.g.sessions_dir .. '/',
    { noremap = true, desc = "Delete session with <name>"})

-- Code marks
-- ----------
-- TODO: move function to lua.
vim.keymap.set("n", "<Leader>dm", ":call Delmarks()<cr>",
    { noremap = true, desc = "Delete all marks in current line"})

-- Moving around
-- -------------
-- Maximize using Ctrl w "
-- Minimize use the default Ctrl w =
-- Problem: All windows will be left with equal size.
-- vim.keymap.set("n", '<C-W>"', "<C-W>_<C-W>|",
--     { noremap = true, desc = "Maximize the current window"})
-- Only solution I've found: the troydm/zoomwintab.vim plugin.
-- Press Ctrl w o to zoom in/out. This overrides the vim's default behavior of
-- closing all windows except current one. We can still do so with the :only
-- normal command.

-- When maximizing, the other windows will be left with their winminheight or
-- winminwidth values. Making it 0, maximization is full-screen.
vim.opt.winminheight = 0
vim.opt.winminwidth = 0

-- Builtin fuzzy-search for files
-- ------------------------------
-- Use the command :find <pattern> and press tab to get recommendations.
-- Example: :find *SEC720*En<tab>
vim.opt.path:append("**")
vim.opt.wildmenu = true
vim.opt.wildignore:append("*.git/*")
vim.opt.wildignore:append("*/build/*,*/build_debug/*,*/build_release/*,*/build_debug_static/*,*/build_release_static/*")

-- ===========================================================================
--                          VIMSCRIPT CONFIGURATION
-- ===========================================================================
-- Load the traditional VimScript configuration file.
-- Do this until I've migrated all changes to lua.
local vimrc = vim.fn.stdpath("config") .. "/vimrc.vim"
vim.cmd.source(vimrc)

-- ===========================================================================
--                                PLUGINS
-- ===========================================================================
require("plugins")
require("telescope_config")
require("blamer_config")
require("lsp_config")
require("dap_config")
--require("dap_ui_config")
require("nvim-tree_config")
require("diffview_config")
require("signature_config")
require("tokyonight_config")
-- require("luasnip.loaders.from_vscode").lazy_load()
-- require("nvim-cmp_config")
require("which-key_config")
require("copilot_config")

-- ===========================================================================
--                                FUNCTIONS
-- ===========================================================================
require("functions/backInTagStack")
