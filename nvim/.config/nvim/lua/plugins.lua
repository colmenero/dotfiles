-- See https://github.com/folke/lazy.nvim

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

plugins = {
  -- User Interface related plugins
  "itchyny/lightline.vim",    -- A nice bar at the bottom of vim
  "folke/tokyonight.nvim",    -- colorscheme
  "nvim-tree/nvim-tree.lua",  -- Nice tree-like file explorer
  -- nvim-web-devicons is an optional but recommended dependency for nvim-tree
  -- and diffview. It requires to install a nerd font (see nerdfonts.com).
  -- I installed it from pacman because downloading it and placing it in
  -- ~/.local/share/fonts was causing me trouble with i3status.
  "nvim-tree/nvim-web-devicons", -- Nice fonts

  -- Git-related plugins
  "mhinz/vim-signify",      -- Line symbol identifying git changes
  "APZelos/blamer.nvim",    -- Git blame at the end of each line
  "sindrets/diffview.nvim", -- Nice diff view

  -- Plugins related to fuzzy finding
  {
    "nvim-telescope/telescope.nvim",  -- Great fuzzy finder with UI
    tag = "0.1.3",
    dependencies = "nvim-lua/plenary.nvim",
  },
  "jremmen/vim-ripgrep",    -- Integration with ripgrep

  -- nvim-cmp completion plugin
  -- "hrsh7th/cmp-nvim-lsp",
  -- "hrsh7th/cmp-buffer",
  -- "hrsh7th/cmp-path",
  -- "hrsh7th/nvim-cmp",
  -- "saadparwaiz1/cmp_luasnip",
  -- "L3MON4D3/LuaSnip",

  -- Copilot completion AI
  -- Configure it on first use with :Copilot setup
  -- Enable copilot with :Copilot enable
  "github/copilot.vim",

  -- Language Server Provider plugins
  "neovim/nvim-lspconfig",   -- Language Server Provider
  "weilbith/nvim-lsp-smag",  -- Tags integrated with lsp-config

  "mfussenegger/nvim-dap",   -- Debugger Adapter Protocol

  -- Other plugins
  "folke/which-key.nvim",      -- Key bindings cheatsheet on typing
  "kshenoy/vim-signature",     -- Visual Aids for marks
  "ray-x/lsp_signature.nvim",  -- Shows the function signature while typing
  "ntpeters/vim-better-whitespace",  -- Highlight whitespaces and easy
                                     -- removal (:StripWhitespace)
  "mbbill/undotree",       -- Visualize undo history
  "dense-analysis/ale",    -- Syntax checking (used only for XML right now)
  "nvim-treesitter/nvim-treesitter",
  "troydm/zoomwintab.vim", -- zoom in/out of a window
  {
    "folke/todo-comments.nvim",
    dependencies = "nvim-lua/plenary.nvim",
  }
}
require("lazy").setup(plugins, opts)

-- =============================================================================
--                    NOTES ON DEBUGGING A SLOW PLUGIN
-- =============================================================================
-- To debug a slow vim, open it, and execute the following commands:
-- :profile start profile.log
-- :profile func *
-- :profile file *
-- [ At this point do slow actions]
-- :profile pause
-- Then quit vim and look at the end of profile.log, where the functions should
-- be ordered by how much time they take.
