local telescope = require("telescope")
local builtin = require('telescope.builtin')
local actions = require('telescope.actions')
local lga_actions = require("telescope-live-grep-args.actions")

telescope.setup{
  defaults = {
    -- Default configuration for telescope goes here:
    -- config_key = value,
    mappings = {
      i = {
        -- map actions.which_key to <C-h> (default: <C-/>)
        -- actions.which_key shows the mappings for your picker,
        -- e.g. git_{create, delete, ...}_branch for the git_branches picker
        ["<C-h>"] = "which_key",
        -- Use Control-j and Control-k to navigate through entries (instead of
        -- the default: Control-n and Control-p)
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous
      },
    },
    layout_strategy = "vertical",
    layout_config = { height = 0.95, width = 0.95},
  },
  pickers = {
    -- Default configuration for builtin pickers goes here:
    -- picker_name = {
    --   picker_config_key = value,
    --   ...
    -- }
    -- Now the picker_config_key will be applied every time you call this
    -- builtin picker
    buffers = {
      mappings = {
        i = { ["<C-d>"] = actions.delete_buffer }
      }
    }
  },
  extensions = {
    -- Your extension configuration goes here:
    -- extension_name = {
    --   extension_config_key = value,
    -- }
    -- please take a look at the readme of the extension you want to configure
    live_grep_args = {
        auto_quoting = true,
    }
  }
}


-- ==========================================================================
--                            KEY BINDINGS
-- ==========================================================================
vim.keymap.set("n", "<leader>fg", builtin.live_grep,
  { desc = "Live grep [Telescope]" })

vim.keymap.set('n', '<leader>ff', builtin.find_files,
  { desc = "Find files [Telescope]" })

vim.keymap.set('n', '<leader>fb', builtin.buffers,
  { desc = "Find buffers [Telescope]" })

--- Lists lsp references for word under cursor
--- You can then open them all in the quickfix window (Ctrl-q) or
--- select some with <Tab> and open only them with (Alt-q).
--
-- Note call using a function so that we can pass an argument.
-- In this case we are using the "vertical" layout and we only want to include
-- the filename in the bottom window.
vim.keymap.set('n', '<leader>fr',
  function() return builtin.lsp_references({show_line=false}) end,
  { desc = "Find lsp references [Telescope]" })

vim.keymap.set('n', '<leader>fm', builtin.marks,
  { desc = "Find marks [Telescope]" })

vim.keymap.set('n', '<leader>fh', builtin.help_tags,
  { desc = "Find help tags [Telescope]" })

vim.keymap.set('n', '<leader>fhi', builtin.command_history,
  { desc = "Find command in history [Telescope]" })

vim.keymap.set("n", "<leader>fp", ":Telescope resume<CR>",
  { desc = ":Telescope resume [Telescope] (find previous)" })
