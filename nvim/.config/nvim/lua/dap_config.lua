-- References:
--   https://github.com/mfussenegger/nvim-dap
--   https://github.com/mfussenegger/nvim-dap/blob/master/doc/dap.txt
--   https://davelage.com/posts/nvim-dap-getting-started/
local dap = require('dap')
local widgets = require('dap.ui.widgets')
local scopes_sidebar = widgets.sidebar(widgets.scopes, {width = 50 })

-- My helper functions
-- ===================
local binary_directory = ''
local target = ''

function get_ctest_output()
  if (binary_directory == '') then
    binary_directory = vim.fn.input("Enter binary directory: ", "build_debug")
  end
  if (target == '') then
    target = vim.fn.input("Enter target: ", "sec0_openssl-3.0Tester")
  end

  local handle = io.popen(
    "ctest -R " .. target .. " -V -N --test-dir " .. binary_directory)
  local ctest_output = handle:read("*a")
  handle:close()

  -- Match against the command (find first whitespace after expected string, in
  -- a greedy fashion)
  _, _, command = string.find(ctest_output, "Test command: (.-)%s")
  _, _, arguments = string.find(ctest_output, "Test command: .-%s(.-)\n")
  _, _, path = string.find(ctest_output, "(LD_LIBRARY_PATH=.-)\n")
  return command, arguments, path
end

function get_ctest_command()
  command, _, _ = get_ctest_output()
  return command
end

function get_ctest_arguments()
  _, arguments, _ = get_ctest_output()
  return arguments
end

function get_ctest_path()
  _, _, path = get_ctest_output()
  return path
end

function get_test_arguments()
  local arguments = {}
  local argumentString = get_ctest_arguments()

  range = vim.fn.input("Enter test range (<test>:<subtest>): ", '')
  if range ~= '' then
    table.insert(arguments, "-range")
    table.insert(arguments, range)
  end

  -- Split the space-separated string of arguments and remove the extra quotes
  for quotedArgument in string.gmatch(argumentString, "%S+") do
    local argument = quotedArgument:gsub('"', '')
    table.insert(arguments, argument)
  end

  return arguments
end

-- Used for debugging the previous functions
-- vim.api.nvim_create_user_command('TestCtest', function() print(get_ctest_path()) end, {})

function conditional_breakpoint()
  local condition = vim.fn.input("Breakpoint condition: ")
  if condition == '' then
    -- regular breakpoint
    return dap.toggle_breakpoint()
  end

  local number = {}

  local numberStr = vim.fn.input("Breakpoint visit number: ")
  if numberStr ~= '' then
    number = numberStr
  end

  return dap.set_breakpoint(condition, number)
end

-- Adapters
-- ========
-- First install lldb (using your distribution package manage) and adjust
-- command to the path of the lldb-vscode executable.
dap.adapters.lldb = {
  type = 'executable',
  command = '/usr/bin/lldb-vscode-14',
  name = 'lldb'
}


-- Debuggers
-- =========
-- Reference: https://github.com/llvm/llvm-project/tree/main/lldb/tools/lldb-vscode#launch-configuration-settings
local lldb = {
  type = "lldb",
  request = "launch",
  name = "Debug",
  cwd = "${workspaceFolder}",
  program = function()
    return get_ctest_command()
  end,
  stopOnEntry = false,
  args = function()
    return get_test_arguments()
  end,
  runInTerminal = false,
  env = function()
    local variables = {}
    table.insert(variables, get_ctest_path())
    return variables
  end,
}

-- If you want to use this for Rust and C, add something like this:

dap.configurations.c = {
  lldb -- different debuggers or more configurations can be used here
}
dap.configurations.rust = {
  lldb -- different debuggers or more configurations can be used here
}

-- Beautification
-- ==============
vim.fn.sign_define('DapBreakpoint',{ text ='🟥', texthl ='', linehl ='', numhl =''})
vim.fn.sign_define('DapBreakpointCondition',{ text ='🔴', texthl ='', linehl ='', numhl =''})
vim.fn.sign_define('DapStopped',{ text ='▶️', texthl ='', linehl ='', numhl =''})


-- Key bindings
-- ============
-- These mappings are difficult to remember but comfortable.
-- As an alternative, use the user command, and repeat with '@:' and '@@'.
vim.keymap.set('n', "<F10>", dap.step_over, { desc = "Step over [DAP]"})
vim.keymap.set('n', "<F11>", dap.step_into, { desc = "Step into [DAP]"})
vim.keymap.set('n', "<F12>", dap.step_out, { desc = "Step out [DAP]"})

-- Custom commands
-- ===============
-- Type '@:' in normal mode to repeat the command (only necessary first time)
-- Type '@@' in normal mode to repeat the command after that.

-- Execution control
-- -----------------
vim.api.nvim_create_user_command('DebugContinue',
  dap.continue,
  { desc = "Continue execution [DAP]"})

vim.api.nvim_create_user_command('DebugRestart',
  -- I don't know why but this has to be called inside a function, or lldb
  -- won't associate the configuration with the running adapter.
  function()
    return dap.restart()
  end,
  { desc = "Restart execution [DAP]"})

vim.api.nvim_create_user_command('DebugStop',
  dap.close,
  { desc = "Stop execution [DAP]"})

-- Breakpoints
-- -----------
vim.api.nvim_create_user_command('Breakpoint',
  function()
    return dap.toggle_breakpoint()
  end,
  { desc = "Toggle breakpoint [DAP]"})

vim.api.nvim_create_user_command('BreakpointWithCondition',
  conditional_breakpoint,
  { desc = "Conditional breakpoint [DAP]"})

vim.api.nvim_create_user_command('BreakpointList',
  dap.list_breakpoints,
  { desc = "List breakpoints [DAP]"})

vim.api.nvim_create_user_command('BreakpointClear',
  dap.clear_breakpoints,
  { desc = "Clear all breakpoints [DAP]"})

-- Frames
-- ------
-- Useful for jumping directly to a frame in the stack.
-- To see the list you can use the debugging terminal as well.
vim.api.nvim_create_user_command('DebugFrames',
  function()
    widgets.centered_float(widgets.frames)
  end,
  { desc = "View the frame stack [DAP]"})

-- Threads
-- ------
-- Useful for jumping directly to a thread.
-- To see the list you can use the debugging terminal as well.
-- Note that nvim-dap only supports at the moment a single stopped thread.
vim.api.nvim_create_user_command('DebugThreads',
  function()
    widgets.centered_float(widgets.threads)
  end,
  { desc = "View the threads [DAP]"})

-- Scopes
-- ------
-- Useful for jumping directly to a thread.
-- To see the list you can use the debugging terminal as well.
vim.api.nvim_create_user_command('DebugScopes',
  scopes_sidebar.toggle,
  { desc = "View the threads [DAP]"})

-- The debugging terminal
-- ----------------------
-- The debugging terminal is very useful and makes other commands not as
-- necessary.
--
-- In the debugging terminal we can do:
--   .frames to show the stack frames
--   .threads to show the threads
--   .up to go up the stack list
--   .down to go down the stack list
--   .c or .continue
--   .n or .next
--   .into
--   .out
--   .goto to jump to specific line
-- And just like in GDB, you'll repeat the previous command if you press enter.
--
-- We can also print the value of specific variables by typing their name on
-- the debugging terminal (no dot preceding).
vim.api.nvim_create_user_command('DebugTerminal',
  function()
    dap.repl.toggle({ height = 10}, 'rightbelow split')
  end,
  { desc = "Toggle the REPL/Debug-console [DAP]"})
