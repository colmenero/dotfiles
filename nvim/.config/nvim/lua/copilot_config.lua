-- Copilot requries a newer node version that ubuntu supports (which to be
-- honest is quite old).
--
-- I'm using nvm to manage the node versions.
-- Initially, I was against changing the default node version (from system to
-- node, which is the updated one). But it ended up being the only solution:
--   nvm alias default node
--
-- I also had to install the "neovim" npm package using the right node version
-- (i.e. `nvm use node` before installing).
--   npm install -g neovim
-- And then run the following command:
vim.g.node_host_prog = '~/.nvm/versions/node/v20.3.0/bin/neovim-node-host'

-- Choose where to enable copilot
vim.g.copilot_filetypes = {
  ["*"] = false,
  ["c"] = true,
  ["c++"] = true,
  ["cpp"] = true,
  ["python"] = true,
  ["rust"] = true,
  ["lua"] = false,
  ["c#"] = true,
  ["go"] = true,
  ["javascript"] = true,
  ["md"] = true,
}

-- The tab is already used by the lsp for completion, so I'm changing the
-- default key binding to
vim.g.copilot_no_tab_map = true
vim.g.copilot_assume_mapped = true

-- imap <silent><script><expr> <C-J> copilot#Accept("\<CR>")
vim.api.nvim_set_keymap('i', '<leader>sa', 'copilot#Accept()',
  {
    noremap = true,
    silent = true ,
    expr = true,
    desc = "Suggestion: accept [Copilot]"
  })
vim.api.nvim_set_keymap('i', '<leader>sn', '<Plug>(copilot-next)',
  {
    noremap = true,
    silent = true ,
    desc = "Suggestion: next [Copilot]"
  })
vim.api.nvim_set_keymap('i', '<leader>sp', '<Plug>(copilot-previous)',
  {
    noremap = true,
    silent = true ,
    desc = "Suggestion: previous [Copilot]"
  })
vim.api.nvim_set_keymap('i', '<leader>sd', '<Plug>(copilot-dismiss)',
  {
    noremap = true,
    silent = true ,
    desc = "Suggestion: dismiss [Copilot]"
  })
