function shiftQuickfixEntry(direction)
  -- get name of current buffer
  local oldName = vim.api.nvim_buf_get_name(0)
  -- Executing cprevious and cnext will raise an exception for the first and
  -- last items. This can happen to me quite frequently when going quickly over
  -- the list, so I prefer to hide the error. lua's pcall function lets us
  -- handle it. I'll only print the error when debugging.
  local status, err  = pcall(vim.cmd, direction)
  if status then
    local newName = vim.api.nvim_buf_get_name(0)
    if string.len(oldName) > 0 and oldName ~= newName then
      vim.cmd('bdelete ' .. oldName)
    end
  elseif string.find(err, "No more items") then
    print("No more entries in this direction. First or last item")
  else
    error(err)
  end
end

function previousQuickfixEntry()
  shiftQuickfixEntry("cprevious")
end
function nextQuickfixEntry()
  shiftQuickfixEntry("cnext")
end
