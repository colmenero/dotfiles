-- Delete the buffer when going back from the tag stack
--   nnoremap <C-t> <C-t>:bdelete#<CR>
-- However, the previous command deletes the buffer as well if the tag is on the
-- same file. In that case we are not going to do anything.
-- You may want to go to the previous position in the jump list (not the tag
-- stack) using Ctrl-o.
function backInTagStack()
  -- get name of current buffer
  local oldName = vim.api.nvim_buf_get_name(0)
  local status, err = pcall(vim.cmd, 'pop')
  if status then
    local newName = vim.api.nvim_buf_get_name(0)
    if string.len(oldName) > 0 and oldName ~= newName then
      vim.cmd('bdelete#')
    end
  elseif string.find(err, "at bottom of tag stack") then
    print("Last entry - already at bottom of tag stack")
  else
    error(err)
  end
end

vim.keymap.set("n", "<C-t>", backInTagStack,
    { noremap = true, desc = "Go back tag stack and delete previous buffer"})
