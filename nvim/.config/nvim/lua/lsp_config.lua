local lspconfig = require('lspconfig')

-- Global configuration.
-- Reference: https://dev.to/vonheikemen/neovim-lsp-setup-nvim-lspconfig-nvim-cmp-4k8e
local lsp_defaults = {
  flags = {
    debounce_text_changes = 150,
  },
  capabilities = require('cmp_nvim_lsp').default_capabilities(
    vim.lsp.protocol.make_client_capabilities()
  ),
  on_attach = function(client, bufnr)
    vim.api.nvim_exec_autocmds('User', {pattern = 'LspAttached'})
  end
}

lspconfig.util.default_config = vim.tbl_deep_extend(
  'force',
  lspconfig.util.default_config,
  lsp_defaults
)

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('User', {
  pattern = 'LspAttached',
  callback = function(ev)

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions

    vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = ev.buf,
      desc = "Display information about symbol [LSP]"})

    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, { buffer = ev.buf,
      desc = "Jump to declaration [LSP]" })

    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = ev.buf,
      desc = "Jump to definition [LSP]" })

    -- Same as previous keybinding but with the mouse. Added for consistency
    -- with other IDE.
    vim.keymap.set('n', '<C-LeftMouse>',
      vim.lsp.buf.definition, { buffer = ev.buf,
      desc = "Jump to definition [LSP]" })

    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, { buffer = ev.buf,
      desc = "Jump to implementation [LSP]" })

    vim.keymap.set('n', 'gs', vim.lsp.buf.signature_help, { buffer = ev.buf,
      desc = "Display signature information [LSP]" })

    -- vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)

    -- Lists all the references in the quickfix lix.
    -- Disable so far in favour of using telescope, see mapping for <leader>fr
    -- You can then open all in quicfix with <Ctrl>q
    -- This would be useful in small screens where telescope doesn't preview
    -- the close-by code, only the affected line.
    -- vim.keymap.set('n', 'ga', vim.lsp.buf.references, opts)

    vim.keymap.set('n', '<leader>fc', function()
      vim.lsp.buf.formatting { async = true }
    end, { buffer = ev.buf, desc = "Format code [LSP]" })
    vim.keymap.set('v', '<leader>fc', function()
      vim.lsp.buf.format { async = true }
    end, { buffer = ev.buf, desc = "Format code [LSP]" })

    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename,
        { buffer = ev.buf, desc = "Rename all references [LSP]"})

    vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action,
        { buffer = ev.buf, desc = "Select code action [LSP]"})

    -- vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    -- vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    -- vim.keymap.set('n', '<space>wl', function()
    --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    -- end, opts)
  end,
})

lspconfig.clangd.setup{
    root_dir = lspconfig.util.root_pattern(
        '../compile_commands.json',
        '.clang-format',
        '.git'
    ),
    filetypes = {
        "c",
        "cpp"
    },
    on_attach = function(client, bufnr)
      lspconfig.util.default_config.on_attach(client, bufnr)
    end
}

lspconfig.rust_analyzer.setup {
    on_attach = function(client, bufnr)
      lspconfig.util.default_config.on_attach(client, bufnr)
    end
}

lspconfig.hls.setup{
  filetypes = { 'haskell', 'lhaskell', 'cabal' },
}
