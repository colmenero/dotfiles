-- Triggers WhichKey when pressing the leader key and waiting for some time
-- (configured by the timeout global variable, 1s by default).
-- If we don't wait that time, WhichKey will not be triggered. Instead it will
-- trigger whatever command directly.
vim.keymap.set("n", "<Leader>", "<Cmd>WhichKey <Space><CR>",
 { silent = true, noremap = true})
