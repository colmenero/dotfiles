local util = require("tokyonight.util")

require("tokyonight").setup({
  style = "night",
  sidebars = { "qf", "vista_kind", "terminal" },

  on_colors = function(colors)
    -- Changes some colors:
    --   - The "hint" color to "orange" color
    --   - The "error" color bright red
    --   - The default color for comments is a bit difficult to read.
    --     I changed it to a bright green, similar to what vscode has.
    colors.hint = colors.orange
    colors.error = "#ff0000"

    -- I changed the opacity (last option) from 0.15 to 0.25 for the add,
    -- delete, and change values. The problem is for modifications ("change" and
    -- "text" can't be be properly seen, so I made it purple with some yellow
    -- color over the modified text (in the same line):
    colors.diff = {
      add = util.darken(colors.green2, 0.25),
      delete = util.darken(colors.red1, 0.25),
      change = util.darken(colors.purple, 0.25),
      text = util.darken(colors.yellow, 0.25),
    }
  end
})


-- Make lightline go along with the colorscheme.
-- Requires loading lightline before tokyonight.
vim.g.lightline = { colorscheme = 'tokyonight'}

-- Enable the color scheme
vim.cmd[[colorscheme tokyonight]]
