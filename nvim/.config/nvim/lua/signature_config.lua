-- See full configuration in https://github.com/ray-x/lsp_signature.nvim

local signature = require("lsp_signature")


signature.setup({
  floating_window = true, -- show hint in a floating window, set to false for virtual text only mode

  -- try to place the floating above the current line when possible Note:
  -- will set to true when fully tested, set to false will use whichever side
  -- has more space this setting will be helpful if you do not want the PUM and
  -- floating win overlap.
  -- Note: the default value for this option was causing me issues.
  floating_window_above_cur_line = false,

  floating_window_off_x = 0, -- adjust float windows x position.
  floating_window_off_y = 5, -- adjust float windows y position.

  handler_opts = {
    border = "rounded"   -- double, rounded, single, shadow, none
  },
})
