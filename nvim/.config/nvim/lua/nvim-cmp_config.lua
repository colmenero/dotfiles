-- Setup nvim-cmp.
-- more info in :help nvim-cmp

local cmp = require('cmp')
local luasnip = require('luasnip')

local select_opts = {behavior = cmp.SelectBehavior.Select}

-- ===========================================================================
-- ===========================================================================
vim.opt.completeopt="preview,menu,menuone,noselect"
-- Limit the maximum number of entries in the menu
-- (the default is complete until full screen)
vim.opt.pumheight = 20

-- ===========================================================================
-- ===========================================================================
cmp.setup({

  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  sources = cmp.config.sources({
    {name = 'nvim_lsp', keyword_length = 3},
    {name = 'buffer', keyword_length = 3},
    {name = 'path'},
    {name = 'luasnip', keyword_length = 2},
  }),
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered()
  },
  formatting = {
    fields = {'menu', 'abbr', 'kind'},
    format = function(entry, item)
      local menu_icon = {
        nvim_lsp = '★',
        luasnip = '∾',
        buffer = '★',
        path = '$',
      }

      item.menu = menu_icon[entry.source.name]
      return item
    end,
  },
  mapping = {
    -- Accept entry by pressing Enter
    ['<CR>'] = cmp.mapping.confirm({ select = true }),

    -- Next entry by pressing Tab or "Up" key arrow
    ['<Up>'] = cmp.mapping.select_prev_item(select_opts),
    ['<Tab>'] = cmp.mapping(function(fallback)
      local col = vim.fn.col('.') - 1

      if cmp.visible() then
        cmp.select_next_item(select_opts)
      elseif col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        fallback()
      else
        cmp.complete()
      end
    end, {'i', 's'}),

    -- Previous entry with Shift-Tab or "Down" key arrow
    ['<Down>'] = cmp.mapping.select_next_item(select_opts),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item(select_opts)
      else
        fallback()
      end
    end, {'i', 's'}),

    -- Scroll up and down the documentation
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
  },
})

-- Disable completion in markdown files
cmp.setup.filetype({ 'markdown' }, {
  sources = {
    { name = 'path' },
  },
  window = {
    completion = { autocomplete = false },
    documentation = cmp.config.disable,
  },
})
