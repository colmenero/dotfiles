local cb = require'diffview.config'.diffview_callback

-- This is so that git diff uses fancy diagonals instead of dash signs for
-- removed code.
vim.opt.fillchars:append { diff = '/' }

-- ===========================================================================
--                             My commands
-- ===========================================================================
-- Toggle the files panel (very useful to be able to clearly see the diff)
--     :DiffviewToggleFiles   ----> pain in the ass. Use <leader>b
--                                  (in my case the leader key is the comma)
-- Open the diff for the next file: Tab
-- Open the diff for the previous file: Shift+Tab
-- Open the local version of the file in a new split in a different tab: gf
--
-- Useful commands:
--   DiffviewFileHistory
--   DiffviewOpen
-- Reference: https://github.com/sindrets/diffview.nvim
--
-- Create a :Diff command that opens diffview showing the changes, organized
-- according to whether they are staged or not.
-- This is only to shorten the :DiffviewOpen command.
vim.api.nvim_create_user_command('Diff', ':DiffviewOpen', {})

-- Create a :PR command that opens diffview for the current branch against
-- develop (same as what bitbucket shows online).
-- Again, this is to avoid typing so much.
vim.api.nvim_create_user_command('PR', ':DiffviewOpen origin/develop...HEAD', {})
vim.api.nvim_create_user_command('PRmain', ':DiffviewOpen origin/main...HEAD', {})
vim.api.nvim_create_user_command('PRmaster', ':DiffviewOpen origin/master...HEAD', {})

-- ==========================================================================
--                            KEY BINDINGS
-- ==========================================================================
-- view the diff
vim.keymap.set("n", "<leader>vd", ":Diff<CR>", { desc = ":Diff [Diffview]" })

-- ===========================================================================
--                             Configuration
-- ===========================================================================
require'diffview'.setup {
  file_panel = {
    win_config = {
      position = "top",          -- One of 'left', 'right', 'top', 'bottom'
      width = 35,                -- Only applies when position is 'left' or 'right'
      height = 15,               -- Only applies when position is 'top' or 'bottom'
    },
  },
  file_history_panel = {
    win_config = {
      position = "bottom",
      width = 35,
      height = 16,
    },
  },
  key_bindings = {
    disable_defaults = false,                   -- Disable the default key bindings
    -- The `view` bindings are active in the diff buffers, only when the current
    -- tabpage is a Diffview.
    view = {
      ["<leader>vf"]  = cb("toggle_files"),       -- Toggle the files panel.
    },
    file_panel = {
      ["<leader>vf"]     = cb("toggle_files"),
    },
  },
  view = {
    merge_tool = {
        layout = "diff3_mixed",
        disable_diagnostics = true,   -- Temporarily disable diagnostics for conflict buffers while in the view.
    }
  }
}

-- ===========================================================================
--                             MERGE TOOL
-- ===========================================================================
-- :h diffview-merge-tool
-- :h diffview-config-view.x.layout
-- Opening a diff view (:Diff) during a merge or a rebase will list the
-- conflicted files in their own section. When opening a conflicted file, it
-- will open in a 3-way diff allowing you to resolve the merge conflicts with
-- the context of the target branch's version, as well as the version from the
-- branch which is being merged.
--
-- `<leader>co`: Choose the OURS version of the conflict (current branch).
-- `<leader>ct`: Choose the THEIRS version of the conflict (incoming branch).
-- `<leader>cb`: Choose the BASE version of the conflict (common ancestor).
-- `<leader>ca`: Choose all versions of the conflict (effectively just deletes
--               the markers, leaving all the content).
-- `dx`: Choose none of the versions of the conflict (delete the conflict
--       region).


-- Moving around:
-- ]c next diff
-- ]x next conflict
-- [c previous diff
-- [x previous conflict
