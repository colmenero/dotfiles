The goal for this file is to keep commonly use commands or key bindings that I
keep forgetting (because I don't have them anywhere or because I have notes all
over the place, but never find where).

Formatting
==========

**Format paragraph to 80 column max width**: gq after visual selection
**Format with lsp_config**: `<Leader>fc` ("format code")

Visual mode
===========

Three types of visual mode:

* **Character-wise**: `v`
* **Line-wise**: `V`
* **Block-wise**: `Ctrl-v`

---
> **Note**: `Ctrl-o` lets you execute a normal mode command in insert mode
> (similar to `:normal`). You can type `Ctrl-o v` to get directly from insert
> mode to character-wise visual mode. The same works for the other modes as
> well.
---

You can start the previously used virtual mode, and on the same highlighted
text with `gv`. With `o` or `O` in visual mode, the cursor jumps from the
beginning to the end of the highlighted block, allowing you to expand the
highlight area.

There are two ways to enter the insert mode from block-wise visual mode: `A` to
enter the text after the cursor or `I` to enter the text before the cursor. Do
not confuse them with `A` (append text at the end of the line) and `I` (insert
text before the first non-blank line) from normal mode.


You can go to the location of the start and the end of the last visual mode
with `` `< `` (start) and `` `> `` (end).

Incrementing/decrementing numbers
---------------------------------
Vim has `Ctrl-x` and `Ctrl-a` commands to decrement and increment numbers. When
used with visual mode, you can increment numbers across multiple lines.

Prepend with a `g` (`g Ctrl-x` and `g Ctrl-a`) to increment the numbers
following a sequence. The sequence by default increases or decreases by 1.
Use `<n>g Ctrl-x` or `<n>g Ctrl-a` to change the step number.

If your vim configuration has the following line:
```
set nrformats+=alpha
```
You'll be able to increment and decrement letters as well.


Navigating the code
===================

Character
---------

* **Left**: `h`
* **Down**: `j`
* **Up**: `k`
* **Right**: `l`
* **Down in a soft-wrapped line**: `gj`
* **Up in a soft-wrapped line**: `gk`

Words
-----

* **Move forward to the beginning of the next word**: `w`
* **Move forward to the beginning of the next WORD**: `W`
* **Move backward to beginning of the previous word**: `b`
* **Move backward to beginning of the previous WORD**: `B`
* **Move forward one word to the end of the next word**: `e`
* **Move forward one word to the end of the next WORD**: `E`
* **Move backward to end of the previous word**: `ge`
* **Move backward to end of the previous WORD**: `gE`

A word is a sequence of characters containing *only* `a-zA-Z0-9_`. A WORD is a
sequence of all characters except white space (a white space means either
space, tab, and EOL).

Lines
-----

* **Go to the first line**: `g`
* **Go to the last line**: `G`
* **Go to line n**: `nG`
* **Go to n% (percentage of file, e.g. 70%) in file**: `n%`
* **Go to the first character in the current line**: `0`
* **Go to the first nonblank char in the current line**: `^`
* **Go to the last non-blank char in the current line**: `g_`
* **Go to the last char in the current line**: `$`
* **Go the column n in the current line**: `n|`
* **Search forward for a match in the same line**: `f<char>`
* **Search backward for a match in the same line**: `F<char>`
* **Search forward for a match in the same line, stopping before match**: `t<char>`
* **Search backward for a match in the same line, stopping before match**: `T<char>`
* **Repeat the last search in the same line using the same direction**: `;`
* **Repeat the last search in the same line using the opposite direction**: `,`

Sentences and paragraphs
------------------------
A sentence ends with either `. ! ?` followed by an EOL, a space, or a tab.
A paragraph begins after each empty line.

* **Jump to the previous sentence**: `(`
* **Jump to the next sentence**: `)`
* **Jump to the previous paragraph**: `{`
* **Jump to the next paragraph**: `}`

Marks
-----
* **Create mark at the current cursor position**: `m<letter>`
  The mark is local to the file if you use a lower-case letter. It can work
  across files if you use an upper case letter.
* **Go to to mark `<letter>` (beginning of line)**: `'<letter>`
* **Go to to mark `<letter>` (position)**: `` `<letter> ``
* **List marks**: `:marks`
* **Delete `<letter>` mark**: `:delm <letter>`.
* **Delete all marks in current line**: `<leader>dm`
* **Delete all marks in current buffer**: `:delm!`
  (you can specify several letters or even a range)
* **Find/list marks (telescope)**: `<leader>fm`

When you list the marks, you get some defaults:
* `''`: Jump back to the last line in current buffer before jump
* ` `` `: Jump back to the last position in current buffer before jump
* `` `[ ``: Jump to beginning of previously changed / yanked text
* `` `] ``: Jump to the ending of previously changed / yanked text
* `` `< ``: Jump to the beginning of last visual selection
* `` `> ``: Jump to the ending of last visual selection
* `` `0 ``: Jump back to the last edited file when exiting vim

The first two are similar to `Ctrl-o` and `Ctrl-i` but more primitive. I don't
think I'll ever learn these mark key-bindings, though.

Windows
-------
* **Go to top of screen**: `H`
* **Go to medium screen**: `M`
* **Go to bottom of screen**: `L`
* **Go n line from top**: `nH`
* **Go n line from bottom**: `nL`
* **Maximize current window**: `Ctrl-w \`
* **Restore after maximization**: `Ctrl-w =`

Scrolling
---------
* **Scroll down half screen**: `Ctrl-D`
* **Scroll up half screen**: `Ctrl-U`
* **Scroll down whole screen**: `Ctrl-F`
* **Scroll up whole screen**: `Ctrl-B`
* **Scroll down a line**: `Ctrl-E`
* **Scroll up a line**: `Ctrl-Y`
* **Bring the current line near the top of your screen**: `zt`
* **Bring the current line to the middle of your screen**: `zz`
* **Bring the current line near the bottom of your screen**: `zb`

Files
-----
* **Go to file**: `:find <pattern><tab>`
* **Go to file (telescope)**: `<leader>ff`
* **Move to the next/previous position**: Use `Ctrl-i` and `Ctrl-o`.
  This goes back and forth in the jump list.
* **Open the jump list**: `:jumps`
* **Function references using telescope+lsp_config**: `<leader>fr`
* **Go to definition**: `gd`

Buffers
-------
* **Go to buffer (telescope)**: `<leader>fb`
* **Go back in the tag stack deleting buffers**: `Ctrl-t`.
  I have it remapped so that it is a combination of `Ctrl-o` and also deletes
  the previous buffer. Just as a sanity mechanism.
* **Go back to the previous buffer**: `Ctrl-^`
* **Delete buffer**: `:bd` but I've also added a mapping in telescope's buffer
  previewer (`<leader>fb`). Type `Ctrl-d` when the cursor is over the buffer
  you want to delete.

Selecting code
==============

TreeSitter
----------
TreeSitter has a feature called incremental selection that allows you to select
text in an smart way (based on language scope). The default keybindings are:

* `gnn` to initialize incremental selection
* `grn` for incrementing to the upper named parent.
* `grm` for decrementing to the previous named node.
* `grc` for scope incremental: increment to the upper scope.

(todo:find easier to remember key bindings)

Searching for text
==================

Built-in text search
--------------------
* `/` for searching text in a buffer. You can restrict it even more by first
  selecting the lines you want to search in using visual selection. Type `n` to
  repeat the search in the same direction or `N` to go backwards. You can also
  start the search with `?` (this is the same as `/` but starting in backwards
  direction).
* `*` Search in current buffer for whole word under cursor forward
* `#` Search in current buffer for whole word under cursor forward
* `g*` Search in current buffer for word under cursor forward
* `g#` Search in current buffer for word under cursor forward

The difference between `*` and `g*` (same for `#` and `g#`) is that `*` only
matches whole words. E.g. try searching the `wholeWord`, using `*` in the
following list: `nothing`, `wholeWordThisIsNot`, `wholeWord`. `*` will find
only the third item (exact word match), `g*` will find the second and third
items.

Ripgrep to the rescue
---------------------
* `:Rg` will search for the word under the cursor
* `:Rg <text>` will search for the given text
* `:Rg <text> <path>` will search for the given text only in the given path
* `:Rg <text> --no-ignore` to search for text without considering the .ignore
* `:Rg <text> -i` to search for text without considering case sensitivity
* `:Rg <text> --type=<type>` to search for text only in those type of files
  (e.g. c, h, rst)
* `<leader>fg` to do a live search (get results while you type)

But typing the `<text>` all the time would be a pain in the ass. We can avoid
it by yanking it in visual mode and then pasting it in the command. Remember to
do:

* `Ctrl-V and select text`
* `y to copy`
* `:Rg <Ctrl-r>"`

This also works for pasting the text we want to search in the live search
telescope input box. We can then choose the files we want in the quickfix with
tab and then press `<Meta-q>` to open them in the quickfix list. Meta is usually
the right alt key. To open in the quickfix list the files NOT selected, press
`<Ctrl-q>` (this is useful for sending all entries).

If for some reason we lost the telescope live grep search and we want to go back
to it, no worries! You can do so with `:Telescope resume`. I've mapped this to
`<leader>fp`.

You can refine telescope live grep search by pressing `<Ctrl-Space>`. Using it
you can filter your results to a specific folder, file extension, etc.
For more information about what you can do in the live grep, use `Ctrl-/>`.

Some useful regex matches
-------------------------

| Pattern | Description                 | Example                             |
| --------| ----------------------------| ----------------------------------- |
| `^`     | Start of line.              |                                     |
| --------| ----------------------------| ----------------------------------- |
| `$`     | End of line.                |                                     |
| --------| ----------------------------| ----------------------------------- |
| `\zs`   | Mark the start and end of a | `foo \zsbar\ze baz` matches the     |
| and     | match. The whole text will  | `bar` in `foo bar baz`. But the     |
| `\ze`   | be requiried for a match,   | whole pattern is still required, so |
|         | but only the text in        | it doesn't match at all for         |
|         | between the `\zs` and `\ze` | `foo bar qux` or `foo bar` or `foo`.|
|         | will be included in it.     |                                     |
| --------| ----------------------------| ----------------------------------- |
| `$`     | End of line.                |                                     |
| --------| ----------------------------| ----------------------------------- |
| `\@=`   | Positive lookahead          | `\(foo \)\@<=bar\( baz\)\@=`        |
|         |                             | behaves the same as                 |
|         |                             | `foo \zsbar\ze baz` (or as          |
|         |                             | `(foo )@<=bar( baz)@=` in very magic|
|         |                             | mode.                               |
| --------| ----------------------------| ----------------------------------- |
| `\@!`   | Negative lookahead          | `/foo\(bar\)\@!` matches `foo`      |
|         |                             | which is not followed by `bar`      |
| --------| ----------------------------| ----------------------------------- |
| `\@<=`  | Positive lookbehind         | `\(mi \)\@<=casa` matches `casa` if |
|         |                             | preceded by `mi `. So it matches    |
|         |                             | only the first `casa` in            |
|         |                             | `mi casa es su casa`                |
| --------| ----------------------------| ----------------------------------- |
| `\@<!`  | Negative lookbehind         | `\(mi \)\@<!casa` matches `casa` if |
|         |                             | not preceded by `mi `. So it maches |
|         |                             | only the second `casa` in           |
|         |                             | `mi casa es su casa`                |
| --------| ----------------------------| ----------------------------------- |

References:

* [jdhao.github.io](https://jdhao.github.io/2018/10/18/regular_expression_nvim/)
* [vim.fandom](https://vim.fandom.com/wiki/Regex_lookahead_and_lookbehind)

Level of magic in a regex expression
------------------------------------
You can simplify regex expressions searching with a "level of magic":

| Level of magic | Prefix | Description                                       |
|----------------|--------|---------------------------------------------------|
| Very magic     | `\v`   | All possible metacharacters are available without |
|                |        | escaping them                                     |
|----------------|--------|---------------------------------------------------|
| Magic          | `\m`   | Only some metacharacters are available without    |
|                |        | escaping them. The others need to be escaped.     |
|                |        | This is the default if no prefix is added.        |
|----------------|--------|---------------------------------------------------|
| Nomagic        | `\M`   | Only some literal characters are available without| 
|                |        | escaping them. The others need to be escaped.     |
|----------------|--------|---------------------------------------------------|
| Very nomagic   | `\V`   | All possible literal characters are available     |
|                |        | without escaping them.                            |

It's complicated to remember the metacharacters, so the most useful modes are
"very magic" (`\v`, no need to escape metacharacters) and "very nomagic" (`\V`,
need to escape metacharacters).

References:
 * [vim.fandom](https://vim.fandom.com/wiki/Simplifying_regular_expressions_using_magic_and_no-magic)
 * [thevaluable.dev](https://thevaluable.dev/vim-regular-expressions-in-depth/)

Replacing text
==============

Current buffer
--------------
`:%s/foo/bar/gc` for replacing `foo` with `bar` in the current buffer, asking
for confirmation. It will replace more than one instance per line (g). You can
restrict the lines to replace in using visual selection or specifing their
numbers with (`:<n0>,<n1>s/foo/bar/gc`).

Several arguments, buffers, tabs, windows, entries in the quickfix list
-----------------------------------------------------------------------
You can use the `:argdo`, `:bufdo`, `:cdo`, `:tabdo`, and `:windo` to apply a
command multiple times (all file arguments in a list, all buffers, all entries
in the quickfix list, all tabs, or all windows in the current tab).

Some examples:

`:bufdo %s/foo/bar/ge | update` will replace `foo` with `bar` for all current
buffers, without reporting an error if the pattern is not found on it (`e`) and
saving the file without requiring confirmation (`update`).

`argdo %s/foo/bar/ge | update` will replace `foo` with `bar` for all files in
the current argument list. You have to first populate the argument list. For
example: `:arg *.h` to add all header files to the list of arguments (you can
display them with `:arg`).

Whole project
-------------
Use `:Rg` to populate the quickfix list, and then use `:cdo` to replace the
found entries. For example:

`:cdo s/foo/bar/g` to replace `foo` with `bar` for all the entries in your
quickfix list.

Don't forget to save the modifications with `:cdo update` (same for `:cfdo`).

**Tip**:to avoid typing `foo` again after the search with `:Rg`, paste the
yanked text again with `<Ctrl-r>"`.

Another option is to use `:Rg` and then call `:cfdo`.
`:cfdo` is similar to `:cdo`. The difference is that with `:cdo` your action is
applied to every entry in the quickfix window, with `:cfdo` your action is
applied to each file in the quickfix window. For example:

`:cfdo %s/foo/bar/g` to replace `foo` with `bar` for all the files in your
quickfix list.

An alternative that doesn't involve `:Rg` is to use `sed` directly:

`sed -i 's/foo/bar/g' <files>` To replace `foo` with `bar` in `<files>` (use
`*` for all files in current working directory).

Git
===

* **git blame line**: `<leader>bl`

Diffview
========

* **diffview open**: `:Diff`, `:DiffviewOpen`, or `<leader>vd`
* **diffview pull request mode**: `:PR`
* **diffview toggle files**: `<leader>vf`
* **diffview next/previous file**: tab / shift+tab
* **diffview open local version of file in new tab**: `gf`

Sessions
========

* **session save**: `<leader>ss`
* **session read**: `<leader>sr`

Panels
======

File panel
----------
* **Toggle file panel**: `<leader>vf`

Vim's built-in file explorer is called `netrw`. Type `:h netrw` to learn more
about it. Some basic commands:

* **Create a new file**: `%`
* **Create a new directory**: `d`
* **Rename a file or directory**: `R`
* **Delete a file or directory**: `D`


Quickfix panel
--------------
* **quickfix next**: `:cn[ext]`
* **quickfix next key binding that deletes buffer as well**: `Ctrl-j`
* **quickfix previous**: `:cp[previous]`
* **quickfix previous key binding that deletes buffer as well**: `Ctrl-k`
* **quickfix close**: `cclose`

Undoing changes
===============

* **Basic undo**: `[number]u` (number is optional and repeats the command the
  number of times)
* **Basic redo**: `[number]Ctrl-r` (number is optional and repeats the command
  the number of times)
* **Undo all last changes in the line**: `U`

---
> **Note**: `u` undoes a single "change" similar to the dot command's change:
> the texts inserted from when you enter the insert mode until you exit it
> count as a change. Instead of changing modes, you can also create breakpoints
> by pressing `Ctrl-g u` in insert mode.
---

Things get more complicated once you do a series of undoes and redoes. You can
find more information [in this
documentation](https://vimdoc.sourceforge.net/htmldoc/usr_32.html). Basically,
when you have more than one possible "timeline" you have to decide which one to
take.

* **You can list all paths with** `:undol`
* **And take one path with**: `:undo <number>`
* You can also use `g+` to go to a newer state and `g-` to go to a previous
  state. The difference with respect to `u` and `Ctrl-r` is that the latter
  traverse only the main nodes of the tree while `g+` and `g-` traverse all
  nodes.

Because keeping track of changes becomes very complicated you have several
options. One is to:

* **Use time descriptors instead (30 seconds, 10 minutes, 1 hour, 2 days - 30s,
  10m, 1h, 2d)**: `:earlier <time>` and `:later <time>`. You can also use
  `:earlier` and `:later` with `<n>f` to mean n saves earlier/later. If you
  don't add any unit, a number of "changes" it's assumed. This is equivalent to
  typing `g-` or `g+` n number of times.

Another option is to install plugins like UndoTree. This makes visualization
and selection much easier.

* **Open the UndoTree panel**: `:UndotreeToggle`

Persistent undo
---------------
Vim can preserve your undo history between sessions with an undo file:
`:wundo <file>` and `:rundo <file>`. This is manual, but can be automated with
the following lines in the configuration:

```
set undodir=<undo_directory>
set undofile
```

This is only necessary in vim, where the feature is off by default. Adding
these lines to the configuration is **not required in neovim**, where the
feature is enabled by default. Nevertheless, you can configure your undo
directory - by default `$XDG_STATE_HOME/nvim/undo/`.

Sessions
========
* **Create a new session**: `<Leader>ss` and then type the name of the session.
* **Restore a previously created session**: `<Leader>sr` and then type the name
  of the session (autocomplete with tab).
* **Delete a previously created session**: `<Leader>sd` and then type the name
  of the session (autocomplete with tab).

Registers
=========

Use `"<reg>` in normal mode or `Ctrl-r<reg>` in insert mode to put the text
from a register.

Vim has 10 types of registers:

1. The unnamed register (`""`). It stores the last text you yanked, changed, or
   deleted. `p` is connected by default to the unnamed register (i.e. `p` is
   equal to `""p`).
2. The numbered registers (`"0-9`). Register 0 stores the last text that you
   yanked. This is useful to be able to paste yanked content after same
   deletions (which updated the unnamed register). Registers 1-9 store changed
   or deleted text that is at least one line long. Register 1 contains the most
   recent text and register 9 the oldest. The numbers are automatically
   incremented when using the dot command (`.`) so you can paste sequentially
   with it.
3. The small delete register (`"-`). Changes or deletions less than one line
   are not stored in the numbered registers 0-9, but in the small delete
   register (`"-`).
4. The named registers (`"a-z`). You have full control over this registers, vim
   doesn't put anything into them unless you tell it to (with `"<char>y` or
   similar motion). If you type the register character in uppercase (e.g. `A`
   instead of `a`), the text will not overwrite the register, but it will be
   appended to it.
5. The read-only registers (`":`, `".`,and `"%`).
   * `.`: Stores the last inserted text
   * `:`: Stores the last executed command-line
   * `%`: Stores the name of current file
6. The alternate file register (`"#`). It contains the name of the last file
   you opened.
7. The expression register (`"=`). It allows you to calculate mathematical
   expressions. For example, `"=1+1<Enter>p` in normal mode or `Ctrl-R =1+1` in
   insert mode will result in the `2` pasted content.
8. The selection registers (`"*` and `"+`). They are useful for copy-pasting
   content from the primary (`*`) and clipboard (`+`) selection. *Primary* is
   usually what you select with your mouse and *clipboard* is usually what you
   copy with `Ctrl-c`.
9. The black hole register (`"_`). The black hole register is like the
   `/dev/null` of registers. Use it to delete or change text without having vim
   storing the content anywhere. E.g. `"_dd`.
10. The last search pattern register (`"/`). It contains the last search
   pattern (either `/` or `?`).

* **View your registers**: `:reg[ister]`
* **Execute macro stored in register**: `@`
* **Paste content from a register**: `"<reg>p` or `:put <reg>`

Avoiding repetition
===================

The dot command
---------------
The dot command (`.`) repeats the last change.

A change is any time you update (add, modify, or delete) the content of the
current buffer, you are making a change. The exceptions are updates done by
command-line commands (the commands starting with `:`) do not count as a
change.

A change also excludes motions because it does not update buffer content.
However, you can combine `.` with `;` or `,`. You can also try to look for
alternatives that modify the buffer content. For example, `gn` searches forward
for the last search pattern and automatically does a visual highlight. `.`
will include it as part of the change.

Macros
------
* **Start recording a macro (in a-z register)**: `q<reg>`
* **Stop recording macro**: `q`
* **Execute macro from register**: `@<reg>`. You can prepend a number to
  execute it a certain number of times. 
* **Execute the last executed macro**: `@@`
* **Execute macro from register over lines**: `:2,3 normal @<reg>`. Replace 2
  and 3 with your beginning and ending lines. You can use visual mode to select
  them. This is sometimes called executing the macro "in parallel", in contrast
  with executing the "in series" by repeting the macro n number of times. One
  of the advantages is that if there is an error while calling the macro for
  one of the lines, the following ones won't by affected. The macro is still
  executed for them.
* **Append to macro**: `q<Reg>`. Same but start recording the macro with the
  uppercase letter for the register.
* **Duplicate macro from one register to another**: `:let @<reg_b> = @<reg_a>`

You can **run a macro recursively**, but make sure that the register is empty
before recording it. You can empty it with `q<reg>q`, which stores an empty
macro. Then you can record your macro and use `@<reg>` to apply the macro in
the macro itself. The stopping condition is on error or on end on file.

You can also **ammend a macro** by first pasting the register's content (`:put
<reg>`), editing it to the desired modification, and then yanking it back to
the register (`"<reg>y$` from the start of the line). You may need to type the
`<Esc>` symbol to represent a change between insert and normal mode. You can do
so by pressing `Ctrl-v` followed by `<Esc>`. `Ctrl-v` is an insert mode
operator to insert the next non-digit character *literally*. Vim will print
`^[`.
