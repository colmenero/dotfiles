source $HOME/.config/nvim/pluginConfig/default.vim

" Line numbers
" -------------------
" Hybrid line numbers (numbers relative except for absolute current line
" number) in normal mode. Absolute line numbers in insert mode.
" Reference: https://jeffkreeftmeijer.com/vim-number/
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
  autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
augroup END

" Preview window
" --------------
" :help preview
" Close preview window when leaving insert mode
" TODO: The buffer is left. How to automatically delete it?
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" Code marks
" ----------
" Delete all marks in current line
function! Delmarks()
    let l:m = join(filter(
       \ map(range(char2nr('a'), char2nr('z')), 'nr2char(v:val)'),
       \ 'line("''".v:val) == line(".")'))
    if !empty(l:m)
        exec 'delmarks' l:m
    endif
endfunction

" ----------------------------------------------------------------------------
source $HOME/.config/nvim/pluginConfig/dds.vim
