My dot files managed with GNU Stow.

Create symlinks for the <folder> directory with:
```
stow -v <folder> --dotfiles
```
Where v means --verbose.
Try first with the -n flag (--no) to simulate the result before running it.

Delete current symlinks for the <folder> directory with:
```
stow -vD <folder> --dotfiles
```

Update symlinks for the <folder> directory with:
```
stow -vR <folder> --dotfiles
```
