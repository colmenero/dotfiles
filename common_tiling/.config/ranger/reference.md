Bookmarking directories
=======================

* Add the bookmark: go to the directory and press `m<letter>`.

* Use the bookmark: press `'<letter>`.

Bookmarks are stored in `.local/share/ranger/bookmarks`.

You can remove them directly deleting their line in the file, or within
range by typing `um<letter>`.
