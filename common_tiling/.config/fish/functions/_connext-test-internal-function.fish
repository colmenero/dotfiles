function _connext-test-internal-function
  if test (count $argv) -eq 0
    echo "More than one argument required"
  end

  set -l pre_command ""
  if test $argv[1] = "debug"
    # "cgdb --args" or "gdb --args"
    set pre_command "cgdb --args"
  else if test $argv[1] = "valgrind"
    # To track "Conditional jump or move depends on uninitialised value(s)"
    # --track-origins=yes
    set pre_command "valgrind --leak-check=full --max-threads=900"
  else if test $argv[1] = "valgrind_server"
    set pre_command "valgrind --leak-check=full --vgdb=yes --vgdb-error=0"
  end

  if test (count $argv) -eq 1; or test "$argv[2]" = "-range"
    # We assume the security target if:
    #    No arguments passed
    #    The arguments start directly with -range
    set test_target "sec0_openssl-3.0Tester"
    set arguments "$argv[2..-1]"
  else
    set test_target "$argv[2]"
    set arguments "$argv[3..-1]"
    # Java exceptions: ctest needs the name of the actual tester executable,
    # not the tester target.
    # In the case of security we replaced the hyphens and dots with
    # underscores (because the others are not supported in the jar archive).
    #
    # Use "--tests" to run a specific test. For example:
    # repo_test security_openssl-1.1.1_javaTester \
    #    "--tests" "com.rti.dds.domain.test.TopicDataTest"
    if string match -q "security_*_javaTester" "$test_target"
      set test_target (string replace - _ $test_target)
      set test_target (string replace . _ $test_target)
    end
  end

  # The output of ctest can get quite high and reach the maximum lenght that
  # the operating system supports for an argument. Create a temporary file to
  # contain the output.
  set -l ctest_environment (mktemp /tmp/connext-test-XXXXX)
  # My Ubuntu workstation has an old version of fish that doesn't support
  # $(command). This is needed because otherwise line endings are not preserved
  # https://github.com/fish-shell/fish-shell/issues/159
  printf '%s\n' (ctest -R $test_target -V -N) > $ctest_environment

  set -l PREVIOUS_LIBRARY_PATH $LD_LIBRARY_PATH
  set -l TEST_LIBRARY_PATH (sed -n -e 's/.*LD_LIBRARY_PATH=//p' $ctest_environment)

  set -gx LD_LIBRARY_PATH "$TEST_LIBRARY_PATH:$PREVIOUS_LIBRARY_PATH"

  set -l test_command (sed -n -e 's/.*Test command: //p' $ctest_environment)
  eval $pre_command $test_command $arguments
  rm $ctest_environment

  set -gx LD_LIBRARY_PATH $PREVIOUS_LIBRARY_PATH
end
