function editdoc
  set -l how_to_edit $EDITOR
  set -l software $argv[1]

  set -l doc_file (_get-my-documentation-file $software)
  $how_to_edit $doc_file
end
