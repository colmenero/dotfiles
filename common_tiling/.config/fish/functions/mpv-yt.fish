# Search for youtube audio directly from terminal
function mpv-yt
  if not type -qs mpv
    echo "mpv is not installed"
    return 0;
  end
  mpv --no-video --ytdl-format=bestaudio ytdl://ytsearch:"$argv"
end
