function connext-valgrind
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Same as connext-test but with valgrind"
    echo "connext-valgrind [TARGET] [ARGS]"
    return
  end

  _connext-test-internal-function "valgrind" $argv
end
