function editconfig
  set -l how_to_edit $EDITOR
  set -l software $argv[1]

  set -l config_file (_get-my-configuration-file $software)
  $how_to_edit $config_file
end
