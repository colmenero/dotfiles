function connext-valgrind-server
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Same as connext-test but with valgrind and gdb"
    echo "connext-valgrind-server [TARGET] [ARGS]"
    return
  end

  _connext-test-internal-function "valgrind_server" $argv
end
