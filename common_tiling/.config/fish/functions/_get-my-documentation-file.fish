function _get-my-documentation-file
  set -l dotfiles "$HOME/.dotfiles"

  # Fish does not have a dictionary-like structure (similar to bash's
  # declarative arrays).
  # In the meantime we do as suggested in:
  # https://stackoverflow.com/questions/40009243/fish-shell-how-to-simulate-or-implement-a-hash-table-associative-array-or
  # and have two separate variables (keys and values).
  set -l doc_keys nvim # 0
  set -a doc_keys ranger # 1
  set -a doc_keys stow # 2
  set -a doc_keys alacritty # 3
  set -a doc_keys rofi # 4

  set -l doc_values "$dotfiles/nvim/.config/nvim/reference.md" # 0
  set -a doc_values "$dotfiles/common_tiling/.config/ranger/reference.md" # 1
  set -a doc_values "$dotfiles/README.md" # 2
  set -a doc_values "$dotfiles/work_tiling/.config/alacritty/reference.md" # 3
  set -a doc_values "$dotfiles/common_tiling/.config/rofi/reference.md" # 4

  set -l software $argv[1]
  if set -l index (contains -i -- $software $doc_keys)
    echo $doc_values[$index]
  end
end
