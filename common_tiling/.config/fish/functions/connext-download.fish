function connext-download
  set -l REPO_CONFIG_DIR "$HOME/rti_connext_dds/1.config"

  # cd build_debug
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Easier conan install"
    echo "connext-download"
    return
  end

  # Create venv from scratch and install pip and conan requirements.
  if test -d .connext-venv
    # If we are in a virtual environment, deactivate it.
    # We probably are in the virtual environment that we want to delete.
    if test -n "$VIRTUAL_ENV"
      deactivate
    end
    rm -rf .connext-venv;
  end

  # In latest ubuntu LTS versions, using python 3.8 requires adding the
  # following PPA: sudo add-apt-repository ppa:deadsnakes/ppa
  # and then install python 3.8: sudo apt install python3.8
  # and the package for the virtual environment: sudo apt install python3.8-venv
  # You may also need the python3-dev and python3.8-dev packages for rti-doozer
  python3.8 -m venv .connext-venv
  source .connext-venv/bin/activate.fish

  # If I don't install wheel, pip complains.
  pip install wheel
  pip install -r ../resource.3.0/python/pip/build-requirements.txt

  # Other tools that may be required for developing:
  # doozer cli, covbutler for coverity, backporter tool, black formatter...
  #
  # # python3 -m black <file>
  pip install rti-doozer --index-url https://repo.rti.com/artifactory/api/pypi/pypi/simple
  pip install covbutler --index-url https://repo.rti.com/artifactory/api/pypi/pypi/simple
  # pip install backporter --index-url https://repo.rti.com/artifactory/api/pypi/pypi/simple
  # pip install diff-match-patch==20200713
  # pip install black

  ../resource.3.0/hooks/clang-format/git-pre-commit-format install

  conan config install https://repo.rti.com/artifactory/thirdparty-local/rti/conan-config.zip

  set -l BUILD_TYPE "Debug"
  if string match -q "build_release*" (basename (pwd))
    set BUILD_TYPE "Release"
  end

  set -l SHARED_LIBRARIES "True"
  if string match -q "*_static" (basename (pwd))
    set SHARED_LIBRARIES "False"
  end

  echo "Conan install: Build type=$BUILD_TYPE and Shared libraries=$SHARED_LIBRARIES"
  grep '^[^#]' $REPO_CONFIG_DIR/download.flags | xargs conan install -s build_type=$BUILD_TYPE -o shared=$SHARED_LIBRARIES
end
