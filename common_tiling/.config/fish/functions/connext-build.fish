function connext-build
  set -l REPO_CONFIG_DIR "$HOME/rti_connext_dds/1.config"

  # cd build_debug
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Easier cmake build"
    echo "connext-build [TARGET: {make help for list; default: install}]"
    return
  end

  source .connext-venv/bin/activate.fish

  set -l BUILD_TYPE "Debug"
  if string match -q "build_release*" (basename (pwd))
    set BUILD_TYPE "Release"
  end

  if test (count $argv) -eq 0
    set TARGET "install"
  else
    # TODO: List of targets?
    set TARGET $argv[1]
  end

  echo "Cmake: Build type=$BUILD_TYPE and target=$TARGET"
  grep '^[^#]' $REPO_CONFIG_DIR/build.flags | xargs cmake --build . --config $BUILD_TYPE --target $TARGET

  # Symlink to compile_commands.json for vscode, if it doesn't already exists.
  # This is not needed for nvim
  if test $BUILD_TYPE = "Debug"; and not test -f ../compile_commands.json
      ln -s (pwd)/compile_commands.json ../compile_commands.json
  end
end
