function _get-my-configuration-file
  set -l dotfiles "$HOME/.dotfiles"

  # Fish does not have a dictionary-like structure (similar to bash's
  # declarative arrays).
  # In the meantime we do as suggested in:
  # https://stackoverflow.com/questions/40009243/fish-shell-how-to-simulate-or-implement-a-hash-table-associative-array-or
  # and have two separate variables (keys and values).
  set -l config_keys viewconfig # 0
  set -a config_keys viewdoc # 1
  set -a config_keys git # 2
  set -a config_keys nvim # 3
  set -a config_keys gdb # 4
  set -a config_keys ssh # 5
  set -a config_keys i3_work # 6
  set -a config_keys terminal_work # 7
  set -a config_keys mutt # 8
  # ------------- Neovim plugins: start -------------
  set -a config_keys diffview # 9
  set -a config_keys lsp # 10
  set -a config_keys telescope # 11
  set -a config_keys packer # 12
  # ------------- Neovim plugins: end -------------

  set -l config_values "$dotfiles/common_tiling/.config/fish/functions/_get-my-configuration-file.fish" # 0
  set -a config_values "$dotfiles/common_tiling/.config/fish/functions/_get-my-documentation-file.fish" # 1
  set -a config_values "$dotfiles/git/dot-gitconfig" # 2
  set -a config_values "$dotfiles/nvim/.config/nvim/init.lua" # 3
  set -a config_values "$dotfiles/gdb/dot-gdbinit" # 4
  set -a config_values "$dotfiles/work_tiling/.ssh/config" # 5
  set -a config_values "$dotfiles/work_tiling/.config/i3/config" # 6
  set -a config_values "$dotfiles/work_tiling/.config/alacritty/alacritty.yml" # 7
  set -a config_values "$dotfiles/personal_tiling/.mutt/muttrc" # 8
  # ------------- Neovim plugins: start -------------
  set -a config_values "$dotfiles/nvim/.config/nvim/lua/diffview_config.lua" # 9
  set -a config_values "$dotfiles/nvim/.config/nvim/lua/lsp_config.lua" # 10
  set -a config_values "$dotfiles/nvim/.config/nvim/lua/telescope_config.lua" # 11
  set -a config_values "$dotfiles/nvim/.config/nvim/lua/plugins.lua" # 12
  # ------------- Neovim plugins: end -------------

  set -l software $argv[1]
  if set -l index (contains -i -- $software $config_keys)
    echo $config_values[$index]
  end
end
