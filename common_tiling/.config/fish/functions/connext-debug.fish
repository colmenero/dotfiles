function connext-debug
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Same as connext-test but with cgdb"
    echo "connext-debug [TARGET] [ARGS]"
    return
  end

  _connext-test-internal-function "debug" $argv
end
