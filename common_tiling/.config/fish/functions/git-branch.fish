# Reference:
# https://stackoverflow.com/questions/15058844/print-branch-description

function git-branch
  # List branches without '*' at the beginning
  for line in (git branch --format='%(refname:short)')
    set -l description (git config branch.$line.description)
    if string length -q $description
      echo "$line | $description"
    else
      echo "$line | No description available"
    end
  end
end
