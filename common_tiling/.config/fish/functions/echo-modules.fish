# List the modules in the tree.
# Requirement: You have to be in the build directory.
function echo-modules
  if test (count $argv) -eq 0
    # List the modules when no input argument is provided
    sed -n -e '/# Core Modules/,$p' ../CMakeLists.txt | grep connextdds_add_module | sed 's/^connextdds_add_module(\(.*\))$/\1/'
  else
    # Otherwise, list the modules and try to match (and highlight) input
    # text. Requires to install ack.
    echo-modules | tr " " "\n" | ack --passthru "$argv[1]"
  end
end
