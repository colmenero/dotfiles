function connext-install
  if test (count $argv) -eq 0
    cmake -P cmake_install.cmake
  else if test $argv[1] = "help"
    echo "Runs the command: cmake -P cmake_install.cmake"
    return
  else if test $argv[1] = "doc"
    cmake -DCMAKE_INSTALL_COMPONENT=htmldocs -P cmake_install.cmake
  else
    echo "Unknown argument"
  end
end
