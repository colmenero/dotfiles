function connext-test
  set -l must_cd_into_module ""

  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Easier ctest -R <tester> -V -N"
    echo "repo_test [TARGET] [ARGS]"
    return
  # Java exceptions: the java tests must be run in their respective modules!
  else if test $argv[1] = "dds_javaTester"
    set must_cd_into_module "dds_java.1.0"
  else if string match -q -- "security_*_javaTester" $argv[1]
    set must_cd_into_module "security.1.0"
  end

  if test -n $must_cd_into_module
    set -l original_directory (pwd)
    cd $must_cd_into_module
    set must_cd_into_module $original_directory
  end

  _connext-test-internal-function "test" $argv

  if test -n $must_cd_into_module
    cd $must_cd_into_module
  end
end
