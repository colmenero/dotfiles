function myabook
  if not type -qs abook
    echo "abook is not installed"
    return 0;
  end
  set -l data_file /home/luis/SharedDrive/Documents/contactsBookmarksAndCalendars/Contacts.abook
  if not test -e $data_file
    echo "abook data file does not exist"
  end
  abook --datafile $data_file
end
