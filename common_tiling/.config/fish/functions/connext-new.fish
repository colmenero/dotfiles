function connext-new
  # Here we have the .bare repository:
  #  git clone ssh://git@bitbucket.rti.com:7999/connext/connextdds.git --bare .bare
  #
  # References:
  #    https://blog.cryptomilk.org/2023/02/10/sliced-bread-git-worktree-and-bare-repo/
  #    https://morgan.cugerone.com/blog/how-to-use-git-worktree-and-in-a-clean-way/
  #    https://morgan.cugerone.com/blog/workarounds-to-git-worktree-using-bare-repository-and-cannot-fetch-remote-branches/

  if test (count $argv) -eq 0
    echo "Create a new worktree to use with our connext tree."
    echo "connext-new <name> [based-branch]"
    return
  end

  set WORKTREE_NAME $argv[1]

  if test (count $argv) -gt 1
    # Create a new worktree based on a existing branch with:
    # connext-new <branch_name> origin/<branch_name>
    set original_branch $argv[2]
  else
    # Start by default from origin/dev/ironside-lts instead of develop
    set original_branch origin/dev/ironside-lts
  end

  cd /home/colmenero/rti_connext_dds/

  # Add a new worktree.
  # It creates a new branch if it doesn't exist or checks out to it if it does.
  # Our branches usually have slashes (e.g. feature/SEC-XXX). Git by default
  # creates a branch from the last component. We override this behavior by
  # passing the name twice: once for the branch and another for the directory.
  # git worktree add -B $WORKTREE_NAME $WORKTREE_NAME


  git worktree add -B $WORKTREE_NAME $WORKTREE_NAME $original_branch

  mkdir $WORKTREE_NAME/build_debug
  mkdir $WORKTREE_NAME/build_release
  mkdir $WORKTREE_NAME/build_debug_static
  mkdir $WORKTREE_NAME/build_release_static
  mkdir $WORKTREE_NAME/stage

  # Use comma as separator for sed. We assume our branches won't have it.
  sed "s,<WORKTREE_NAME>,$WORKTREE_NAME,g" /home/colmenero/.dotfiles/work_tiling/rti_connext_dds/dot-envrc-template > $WORKTREE_NAME/stage/.envrc

  # Unset upstream branch
  cd $WORKTREE_NAME && git branch --unset-upstream
  cd -
end
