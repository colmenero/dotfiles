function remindme
  if not type -qs remind
    echo "remind is not installed"
    return 0;
  end
  set -l include_file /home/luis/SharedDrive/Documents/contactsBookmarksAndCalendars/includes.rem
  if not test -e $include_file
    echo "remind include file does not exist"
  end
  remind $argv $include_file
end
