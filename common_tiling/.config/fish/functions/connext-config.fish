function connext-config
  set -l REPO_CONFIG_DIR "$HOME/rti_connext_dds/1.config"

  # cd build_debug
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Easier cmake configuration"
    echo "connext-config"
    return
  end

  source .connext-venv/bin/activate.fish

  set -l BUILD_TYPE "Debug"
  if string match -q "build_release*" (basename (pwd))
    set BUILD_TYPE "Release"
  end

  set -l SHARED_LIBRARIES "True"
  if string match -q "*_static" (basename (pwd))
    set SHARED_LIBRARIES "False"
  end

  echo "Cmake: Build type=$BUILD_TYPE and Shared libraries=$SHARED_LIBRARIES"
  grep '^[^#]' $REPO_CONFIG_DIR/config.flags | xargs cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DBUILD_SHARED_LIBS=$SHARED_LIBRARIES
end
