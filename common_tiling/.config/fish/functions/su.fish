# Make su launch fish
# Reference: https://wiki.archlinux.org/title/fish
function su
 command su --shell=/usr/bin/fish $argv
end
