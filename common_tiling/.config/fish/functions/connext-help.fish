function connext-help
  echo "----------------------- Clone repository -----------------------"
  echo "0. Run `connext_new` in the root directory"
  echo "----------------------- Build repository -----------------------"
  echo "Build commands (in build directory)"
  echo "1. connext-download"
  echo "2. connext-config"
  echo "3. connext-build"
  echo "4. connext-install"
  echo "----------------------- Test repository -----------------------"
  echo "Test commands (in build directory)"
  echo "- Run test: `connext-test`"
  echo "- Run test with cgdb: `connext-debug`"
  echo "- Run test with valgrind: `connext-valgrind`"
  echo "- Run test with valgrind server (use with gdb): `connext-valgrind-server`"

  if test -e .connext-venv/bin/activate
    source .connext-venv/bin/activate.fish
  else
    echo " No virtual environment found"
  end
end

