function split-file
  set -l input_file $argv[1]
  set -l number_of_files $argv[2]

  set -l output_prefix $input_file
  set -l number_of_lines (wc -l < $input_file)
  set -l lines_per_file (math ceil $number_of_lines/$number_of_files)
  split -da 4 -l $lines_per_file $input_file $output_prefix --additional-suffix=".part"
end
