function viewconfig
  # View using glow with paging enabled
  set -l how_to_view "less"
  set -l software $argv[1]

  set -l config_file (_get-my-configuration-file $software)
  eval "$how_to_view $config_file"
end
