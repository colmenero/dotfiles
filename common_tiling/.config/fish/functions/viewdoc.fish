function viewdoc
  # View using glow with paging enabled
  set -l how_to_view "glow -p"
  set -l software $argv[1]

  if not type -qs $how_to_view
    # My personal documentation files are in markdown. That's why the default
    # viewer is "glow". However, if it's not installed, default to "less".
    set -l how_to_view "less"
  end

  set -l doc_file (_get-my-documentation-file $software)
  eval "$how_to_view $doc_file"
end
