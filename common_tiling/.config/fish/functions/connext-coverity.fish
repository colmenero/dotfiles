
# ##############################################################################
# Needs environment to have the proper variables:
# cd into the connextN folder for letting direnv handle them.
function connext-coverity
  set coverity_install_dir "/home/colmenero/cov-analysis-linux64-2023.6.0/"
  # TODO: add option --reference-branch
  # Note to self: coverity uses the x86_64Linux.cmake architecture, just
  #               as jenkins does.
  # cd build_debug
  if test (count $argv) -gt 0; and test $argv[1] = "help"
    echo "Easier covbutler"
    echo "Analize the full tree: connext_coverity"
    echo "Analize a target: connext_coverity target"
    echo "Analize some files for a target: connext_coverity target files"
    return
  end


  source .connext-venv/bin/activate.fish

  set -l build_type "debug"
  if string match -q "build_release*" (basename (pwd))
    set build_type "release"
  end

  # Analyze the full tree by default.
  set -l target ""
  if not test (count $argv) -eq 0
    # Analyze only the specified target.
    set target "--target $argv[1]"
  end

  if test (count $argv) -eq 0; or test (count $argv) -eq 1
    # The result will be in build_debug/output_full.basic_<date>
    # There is no need for running:
    # cov-format-errors --dir build_debug/cov.cache/idir --html-output build_debug/cov.cache/idir/html
    # Change the kind of analysis according to the errors. It will usually be
    # full.basic, but other options are full.secure, full.certc, and desktop.
    # full.certc is required to get the CERTC-C issues.
    #
    # Why doesn't the target argument work for the connext-coverity script?
    # nddssecurity_openssl-3.0
    covbutler --kind full.basic --architecture $PCARCH \
        --build-mode $build_type \
        --source-dir .. --cov-home $coverity_install_dir \
        --target nddsc
  else if test (count $argv) -eq 2
    # TODO: add --compare to see only changes with respect to develop
    # TODO: add --save-output <file> so that the results are not lost if we
    #       close the terminal.
    covbutler --kind desktop --architecture $PCARCH \
        --source-dir .. --cov-home $coverity_install_dir \
        --auth $coverity_install_dir/../auth-key.txt \
        $target --files $argv[2]
  else
    echo "Didn't expect more than two arguments"
  end
end
