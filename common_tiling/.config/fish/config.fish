# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end

# If amixer installed
#   mute mic and disable auto-mute (just in case)
if status is-login; and type -qs amixer
  amixer set Capture nocap
  amixer set "Auto-Mute Mode" Disabled
end

if status is-interactive
    # Commands to run in interactive sessions can go here
end
# ############################################################################
# GLOBAL VARIABLES
# ############################################################################
set -Ux VISUAL nvim
set -Ux EDITOR $VISUAL
set -Ux BROWSER firefox

# ############################################################################
# ABBREVIATIONS
# ############################################################################
# Neomutt
abbr --add mutt neomutt

# Neovim
abbr --add nv nvim
abbr --add vim nvim
abbr --add vimdiff nvim -d

# lsd: an improved ls command
# Reference: https://github.com/Peltoche/lsd
if type -qs lsd
  abbr --add ls lsd
  abbr --add ll lsd -l
  abbr --add la lsd -a
  abbr --add lla lsd -la
  abbr --add lt lsd --tree
end

# bat: an improved bat command
# Reference: https://github.com/sharkdp/batcat
#
# Note: ubuntu installs bat as batcat
if not type -qs bat; and type -qs batcat
  alias bat="batcat"
end
if type -qs bat
  abbr cat bat --paging=never
  abbr less bat
end


# ripgrep: a better rgrep
# Reference: https://github.com/BurntSushi/ripgrep
#
# The ripgrep command is "rg", which is shorter than "rgrep".
# I just have the alias here as a reminder to use ripgrep.
if type -qs rg
  abbr rgrep rg
end

# fd: a better find
# Reference: https://github.com/sharkdp/fd
#
# This is for finding & listing files in your (real) file system.
#
# Use plocate (in combination with updatedb) to find files in a much faster,
# but less flexible, way. plocate uses a pre-built database.
#
# The fd command is already shorter than "find".
# I just have the alias here as a reminder to use the former.
#
# Note: ubuntu installs fd as fdfind
if type -qs fdfind
  alias fd="fdfind"
end
if type -qs fd
  abbr find fd
end

# difftastic: a better diff
# Reference: https://github.com/Wilfred/difftastic
#
if type -qs difft
  abbr diff difft
end

# ############################################################################
# PROGRAM CONFIGURATION
# ############################################################################
# direnv: set project-specific environment variables
if type -qs direnv
  direnv hook fish | source
end

# starship shell prompt
if type -qs starship
  starship init fish | source
end

# zoxide: smarter cd
# You can also use 'cdi' after installing fzf. This will enable fuzzy finding
# completion after pressing the tab key.
if type -qs zoxide
  zoxide init --cmd cd fish | source
end
# ############################################################################
# COLOR SCHEME
# ############################################################################
set -l fish_colorscheme_file $HOME/.config/fish/tokyonight_night.fish
if test -e $fish_colorscheme_file
  source $fish_colorscheme_file
end

# ############################################################################
# PATHS
# ############################################################################
fish_add_path ~/bin
fish_add_path ~/.local/bin
# ############################################################################

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin /home/luis/.ghcup/bin $PATH # ghcup-env
